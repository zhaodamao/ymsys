package com.ymsys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.sys.annotation.DataFilter;
import com.ymsys.dao.SysUserDao;
import com.ymsys.entity.SysRoleEntity;
import com.ymsys.entity.SysUserEntity;
import com.ymsys.service.SysRoleService;
import com.ymsys.service.SysUserRoleService;
import com.ymsys.service.SysUserService;
import com.ymsys.shiro.ShiroUtils;
import com.ymsys.utils.Constant;
import com.ymsys.utils.PageUtils;
import com.ymsys.utils.Query;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("sysUserService")
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUserEntity> implements SysUserService {

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysRoleService sysRoleService;

    @Override
    public List<Long> queryAllMenuId(Long userId) {
        return baseMapper.queryAllMenuId(userId);
    }

    @Override
    @DataFilter(subDept = true, user = false)
    public PageUtils queryPage(Map<String, Object> params) {

        String username = (String)params.get("username");
        Page<SysUserEntity> page = this.selectPage(
                new Query<SysUserEntity>(params).getPage(),
                new EntityWrapper<SysUserEntity>()
                        .like(StringUtils.isNotBlank(username),"username", username)
                        .addFilterIfNeed(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER))
        );

        List<SysUserEntity>listUser = page.getRecords();
        for(int i = 0 ;i < listUser.size();++i){
            List<Long> roleIdList = sysUserRoleService.queryRoleIdList(listUser.get(i).getUserId());
            List<JSONObject>roleList = new ArrayList<JSONObject>();
            for(int j = 0 ; j < roleIdList.size(); ++ j){
                JSONObject obj = new JSONObject();
                SysRoleEntity sysRoleEntity = sysRoleService.selectById(roleIdList.get(j));
                obj.put("roleId",sysRoleEntity.getRoleId());
                obj.put("roleName",sysRoleEntity.getRoleName());
                roleList.add(obj);
            }

            listUser.get(i).setRoleList(roleList);
        }

        return new PageUtils(page);
    }

    @Override
    public boolean updatePassword(Long userId, String password, String newPassword) {
        SysUserEntity userEntity = new SysUserEntity();
        userEntity.setPassword(newPassword);
        return this.update(userEntity,
                new EntityWrapper<SysUserEntity>().eq("user_id", userId).eq("password", password));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(SysUserEntity user) {
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        //sha256加密
        String salt = RandomStringUtils.randomAlphanumeric(20);
        user.setSalt(salt);
        user.setPassword(ShiroUtils.sha256(user.getPassword(), user.getSalt()));
        this.insert(user);

        //保存用户与角色关系
        sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(SysUserEntity user) {
        if(StringUtils.isBlank(user.getPassword())){
            user.setPassword(null);
        }else{
            user.setPassword(ShiroUtils.sha256(user.getPassword(), user.getSalt()));
        }
        this.updateById(user);

        //保存用户与角色关系
        sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
    }
}
