package com.ymsys.service.impl;

import com.ymsys.service.SysFileService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service("sysFileService")
public class SysFileServiceImpl implements SysFileService {
    private String directory = "D:/mygit/YMSYS/ymsys/langren/file";
    @Override
    public void fileUpload(MultipartFile[] files) {
        File filePath = new File(directory);
        for (MultipartFile file : files){
            String packageUrl = directory + "/" + file.getOriginalFilename();
            FileOutputStream fs = null;
            try {
                fs = new FileOutputStream(packageUrl);
                fs.write(file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
