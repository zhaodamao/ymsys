package com.ymsys.service;

import com.baomidou.mybatisplus.service.IService;
import com.ymsys.entity.SysUserEntity;
import com.ymsys.utils.PageUtils;

import java.util.List;
import java.util.Map;


/**
 * 系统用户
 *
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-13 16:25:20
 */
public interface SysUserService extends IService<SysUserEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 修改密码
     * @param userId       用户ID
     * @param password     原密码
     * @param newPassword  新密码
     */
    boolean updatePassword(Long userId, String password, String newPassword);

    /**
     * 保存用户
     */
    void save(SysUserEntity user);

    /**
     * 修改用户
     */
    void update(SysUserEntity user);

    /**
     * 查询用户的所有菜单ID
     */
    List<Long> queryAllMenuId(Long userId);
}

