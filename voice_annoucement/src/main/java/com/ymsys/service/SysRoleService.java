package com.ymsys.service;

import com.baomidou.mybatisplus.service.IService;
import com.ymsys.entity.SysRoleEntity;
import com.ymsys.utils.PageUtils;

import java.util.Map;

/**
 * 角色
 *
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-13 16:25:20
 */
public interface SysRoleService extends IService<SysRoleEntity> {

    //PageUtils queryPage(Map<String, Object> params);
    PageUtils queryPage(Map<String, Object> params);

    void save(SysRoleEntity role);

    void update(SysRoleEntity role);

    //void deleteBatch(Long[] roleIds);
}

