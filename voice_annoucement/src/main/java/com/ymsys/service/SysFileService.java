package com.ymsys.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * 文件服务器
 *
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-13 16:25:20
 */
public interface SysFileService {
    void  fileUpload(MultipartFile files[]);
}

