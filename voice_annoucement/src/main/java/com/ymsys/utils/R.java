/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.ymsys.utils;
import com.sun.net.httpserver.HttpsServer;
import com.ymsys.myenum.RetEnum;

import javax.servlet.http.HttpServlet;

/**
 * 返回数据
 *
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date  2018-11-13 16:25:20
 */
public class R {
	private int code;
	private String msg;
	private Object data;

	public R(){
		this.code = RetEnum.SERVICE_OK.getCode();
		this.msg = RetEnum.SERVICE_OK.getMsg();
	}

	public R(Object data){
		this.data = data;
	}


	public R(RetEnum retEnum){
		this.code = retEnum.getCode();
		this.msg = retEnum.getMsg();
	}

	public R(RetEnum retEnum,Object data){
		this.code = retEnum.getCode();
		this.msg = retEnum.getMsg();
		this.data =data;
	}

	public R(int code,String msg){
		this.code = code;
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public Object getData() {
		return data;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "AjaxResult [code=" + code + ", msg=" + msg + ", result=" + data + "]";
	}
}
