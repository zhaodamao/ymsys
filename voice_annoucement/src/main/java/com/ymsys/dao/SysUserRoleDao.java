package com.ymsys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ymsys.entity.SysUserRoleEntity;

import java.util.List;

/**
 * 用户与角色对应关系
 * 
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-13 16:25:20
 */
public interface SysUserRoleDao extends BaseMapper<SysUserRoleEntity> {
    /**
     * 根据用户ID，获取角色ID列表
     */
    List<Long> queryRoleIdList(Long userId);

    /**
     * 根据角色ID数组，批量删除
     */
    int deleteBatch(Long[] roleIds);
}
