package com.ymsys.myenum;

public enum RetEnum {

    SERVICE_OK(0, "服务正常"),
    SERVICE_UNKNOW_ERROR(10001, "未知错误"),
    SERVICE_USER_PASSWOLD_ERROR(10002,"用户名或密码错误"),
    SERVICE_USER_LOCK(10003,"账号已被锁定,请联系管理员"),
    SERVICE_USER_CHECK_FAILD(10004,"账户验证失败"),
    SERVICE_HAVE_DATA_DB(10005,"数据库中已存在该记录"),
    SERVICE_HAVE_NO_PERMISSION(10006,"没有权限，请联系管理员授权"),
    SERVICE_UNKNOW_EXCEPTION(10007,"未知异常"),
    SERVICE_OLD_PASSWORD_ERROR(10008,"原密码不正确"),
    SERVICE_CANT_DELETE_SYS_ADMIN(10009,"系统管理员不能删除"),
    SERVICE_CANT_DELETE_CUR_USER(10010,"当前用户不能删除"),
    SERVICE_DELETE_SUB_DEPT_BEFORE(10011,"请先删除子部门"),
    SERVICE_SYS_MUNE_CANNOT_DELETE(10012,"系统菜单不能删除"),
    SERVICE_DELETE_SUB_MUNE_OR_BUTTON(10013,"请先删除子菜单或按钮"),


    SERVICE_END(99999,"枚举结束");




    private int code;
    private String msg;

    private RetEnum(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode(){
        return this.code;
    }

    public String getMsg(){
        return this.msg;
    }
}
