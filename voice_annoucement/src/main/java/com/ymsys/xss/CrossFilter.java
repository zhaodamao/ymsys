package com.ymsys.xss;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by liulq on 2017-11-29 .
 */
@WebFilter(filterName="crossFilter",urlPatterns="/*")
public class CrossFilter extends HttpServlet implements Filter {

    /**
     * 序列ID，用来监测控制版本更新的
     */
    private static final long serialVersionUID = 1L;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    //设置其他IP地址的机器可以直接访问到这个项目的后端
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.setHeader("Access-Control-Allow-Methods","GET,HEAD,OPTIONS,POST,PUT");
        httpResponse.setHeader("Access-Control-Allow-Origin", servletRequest.getHeader("Origin"));
        httpResponse.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
        httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
        if (servletRequest.getMethod().equals( "OPTIONS" )) {
            httpResponse.setStatus( 200 );
            return;
        }

        chain.doFilter(request, httpResponse);
    }

}