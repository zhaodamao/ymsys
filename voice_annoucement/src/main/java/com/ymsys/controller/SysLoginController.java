/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.ymsys.controller;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.ymsys.entity.SysMenuEntity;
import com.ymsys.entity.SysUserEntity;
import com.ymsys.myenum.RetEnum;
import com.ymsys.service.SysMenuService;
import com.ymsys.shiro.ShiroUtils;
import com.ymsys.utils.PageUtils;
import com.ymsys.utils.R;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 登录相关
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年11月10日 下午1:15:31
 */
@RestController
public class SysLoginController {

	@Autowired
	private Producer producer;
	@Autowired
	private SysMenuService sysMenuService;

	@RequestMapping("captcha")
	public void captcha(HttpServletResponse response)throws IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");

        //生成文字验证码
        String text = producer.createText();
        //生成图片验证码
        BufferedImage image = producer.createImage(text);
        //保存到shiro session
        ShiroUtils.setSessionAttribute(Constants.KAPTCHA_SESSION_KEY, text);
        
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
	}
	
	/**
	 * 登录
	 */

	@PostMapping("/login")
	public Object login(@RequestBody Map<String,String> map) {

		String username = map.get("username");
		String password = map.get("password");
		//String kaptcha = ShiroUtils.getKaptcha(Constants.KAPTCHA_SESSION_KEY);
		//if(!captcha.equalsIgnoreCase(kaptcha)){
		//	return R.error("验证码不正确");
		//}
		
		try{

			System.out.print("user:"+username+"  pw:"+password);
			Subject subject = ShiroUtils.getSubject();
			UsernamePasswordToken token = new UsernamePasswordToken(username, password);
			subject.login(token);
		}catch (UnknownAccountException e) {
			return new R(RetEnum.SERVICE_UNKNOW_ERROR);
		}catch (IncorrectCredentialsException e) {
			return new R(RetEnum.SERVICE_USER_PASSWOLD_ERROR);
		}catch (LockedAccountException e) {
			return new R(RetEnum.SERVICE_USER_LOCK);
		}catch (AuthenticationException e) {
			return new R(RetEnum.SERVICE_USER_CHECK_FAILD);
		}
		return new R(RetEnum.SERVICE_OK);
	}
	
	/**
	 * 退出
	 */

	@PostMapping(value = "logout")
	public R logout() {
		ShiroUtils.logout();
		return new R(RetEnum.SERVICE_OK);
	}

	@GetMapping(value = "/testRead")
	public R testRead() {
		List<SysMenuEntity> menuList = sysMenuService.selectList(null);
		for(SysMenuEntity sysMenuEntity : menuList){
			SysMenuEntity parentMenuEntity = sysMenuService.selectById(sysMenuEntity.getParentId());
			if(parentMenuEntity != null){
				sysMenuEntity.setParentName(parentMenuEntity.getName());
			}
		}
		return new R(RetEnum.SERVICE_OK,menuList);
	}

	@PostMapping(value = "/testWrite")
	public R testWrite(@RequestBody SysMenuEntity menu) {
		//数据校验
		sysMenuService.insert(menu);
		return new R(RetEnum.SERVICE_OK);
	}
}
