package com.ymsys.controller;

import com.alibaba.fastjson.JSONObject;
import com.sys.group.AddGroup;
import com.sys.group.UpdateGroup;
import com.ymsys.entity.SysRoleEntity;
import com.ymsys.entity.SysUserEntity;
import com.ymsys.myenum.RetEnum;
import com.ymsys.service.SysRoleService;
import com.ymsys.service.SysUserRoleService;
import com.ymsys.service.SysUserService;
import com.ymsys.shiro.ShiroUtils;
import com.ymsys.utils.Assert;
import com.ymsys.utils.PageUtils;
import com.ymsys.utils.R;
import com.ymsys.utils.ValidatorUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 系统用户
 *
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-13 16:25:20
 */

@RestController
public class SysUserController  extends AbstractController{
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;
    @Autowired
    private SysRoleService sysRoleService;


    /**
     * 获取管理员列表
     *
     */
    @PostMapping("/user/list")
    //@RequiresPermissions("sys:user:list")
    public R list(@RequestBody Map<String, Object> params){
        PageUtils page = sysUserService.queryPage(params);
        return new R(RetEnum.SERVICE_OK,page);
    }

    /**
     * 获取登录的用户信息
     */
    @GetMapping("/user/info")
    public R info(){
        SysUserEntity sysUserEntity = getUser();
        //获取用户所属的角色列表
        List<Long> roleIdList = sysUserRoleService.queryRoleIdList(sysUserEntity.getUserId());
        List<JSONObject>roleList = new ArrayList<JSONObject>();
        for(int i = 0 ; i < roleIdList.size(); ++ i){
            JSONObject obj = new JSONObject();
            SysRoleEntity sysRoleEntity = sysRoleService.selectById(roleIdList.get(i));
            obj.put("roleId",sysRoleEntity.getRoleId());
            obj.put("roleName",sysRoleEntity.getRoleName());
            roleList.add(obj);
        }

        sysUserEntity.setRoleList(roleList);
        return new R(RetEnum.SERVICE_OK,sysUserEntity);
    }

    /**
     * 用户信息
     */
    @GetMapping("/user/info/{userId}")
    @RequiresPermissions("sys:user:info")
    public R info(@PathVariable("userId") Long userId){
        SysUserEntity user = sysUserService.selectById(userId);

        //获取用户所属的角色列表
        List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
        List<JSONObject>roleList = new ArrayList<JSONObject>();
        for(int i = 0 ; i < roleIdList.size(); ++ i){
            JSONObject obj = new JSONObject();
            SysRoleEntity sysRoleEntity = sysRoleService.selectById(roleIdList.get(i));
            obj.put("roleId",sysRoleEntity.getRoleId());
            obj.put("roleName",sysRoleEntity.getRoleName());
            roleList.add(obj);
        }

        user.setRoleList(roleList);
        return new R(RetEnum.SERVICE_OK,user);
    }

    /**
     * 保存用户
     */
    //@SysLog("保存用户")
    @PostMapping("/user/save")
    //@RequiresPermissions("sys:user:save")
    public R save(@RequestBody SysUserEntity user){
        ValidatorUtils.validateEntity(user, AddGroup.class);
        sysUserService.save(user);
        return new R(RetEnum.SERVICE_OK);
    }

    /**
     * 修改用户
     */
    //@SysLog("修改用户")
    @PostMapping("/user/update")
    //@RequiresPermissions("sys:user:update")
    public R update(@RequestBody SysUserEntity user){
        ValidatorUtils.validateEntity(user, UpdateGroup.class);
        sysUserService.update(user);
        return new R(RetEnum.SERVICE_OK);
    }

    /**
     * 修改登录用户密码
     */
    @PostMapping("/user/password")
    public R password(String password, String newPassword){
        Assert.isBlank(newPassword, "新密码不为能空");

        //原密码
        password = ShiroUtils.sha256(password, getUser().getSalt());
        //新密码
        newPassword = ShiroUtils.sha256(newPassword, getUser().getSalt());

        //更新密码
        boolean flag = sysUserService.updatePassword(getUserId(), password, newPassword);
        if(!flag){
            return new R(RetEnum.SERVICE_USER_PASSWOLD_ERROR);
        }

        return new R(RetEnum.SERVICE_OK);
    }

    /**
     * 删除用户
     */
    //@SysLog("删除用户")
    @PostMapping("/user/delete")
    //@RequiresPermissions("sys:user:delete")
    public R delete(@RequestBody Long[] userIds){
        if(ArrayUtils.contains(userIds, 1L)){
            return new R(RetEnum.SERVICE_CANT_DELETE_SYS_ADMIN);
        }

        if(ArrayUtils.contains(userIds, getUserId())){
            return  new R(RetEnum.SERVICE_CANT_DELETE_CUR_USER);
        }

        sysUserService.deleteBatchIds(Arrays.asList(userIds));
        return new R(RetEnum.SERVICE_OK);
    }
}
