package com.ymsys.controller;

import com.ymsys.entity.SysRoleEntity;
import com.ymsys.myenum.RetEnum;
import com.ymsys.service.SysRoleMenuService;
import com.ymsys.service.SysRoleService;
import com.ymsys.utils.PageUtils;
import com.ymsys.utils.R;
import com.ymsys.utils.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 角色
 *
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-13 16:25:20
 */
@RestController
public class SysRoleController {
    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    /**
     * 角色列表
     */
    @PostMapping("/role/list")
    //@RequiresPermissions("sys:role:list")
    public R list(@RequestBody Map<String, Object> params){
        PageUtils page = sysRoleService.queryPage(params);
        return new R(RetEnum.SERVICE_OK,page);
    }

    /**
     * 角色列表
     */
    @GetMapping ("/role/select")
    //@RequiresPermissions("sys:role:select")
    public R select(){
        List<SysRoleEntity> list = sysRoleService.selectList(null);
        return new R(RetEnum.SERVICE_OK,list);
    }

    /**
     * 角色信息
     */
    @GetMapping("/role/info/{roleId}")
    //@RequiresPermissions("sys:role:info")
    public R info(@PathVariable("roleId") Long roleId){
        SysRoleEntity role = sysRoleService.selectById(roleId);

        //查询角色对应的菜单
        List<Long> menuIdList = sysRoleMenuService.queryMenuIdList(roleId);
        role.setMenuIdList(menuIdList);

        return new R(RetEnum.SERVICE_OK,role);
    }

    /**
     * 保存角色
     */
    @PostMapping("/role/save")
    //@RequiresPermissions("sys:role:save")
    public R save(@RequestBody SysRoleEntity role){
        ValidatorUtils.validateEntity(role);
        sysRoleService.save(role);
        return new R(RetEnum.SERVICE_OK);
    }

    /**
     * 修改角色
     */
    @PostMapping("/role/update")
    //@RequiresPermissions("sys:role:update")
    public R update(@RequestBody SysRoleEntity role){
        ValidatorUtils.validateEntity(role);

        sysRoleService.update(role);

        return new R(RetEnum.SERVICE_OK);
    }

    /**
     * 删除角色
     */
    @PostMapping("/role/delete")
    //@RequiresPermissions("sys:role:delete")
    public R delete(@RequestBody Long[] roleIds){
        sysRoleService.deleteBatchIds(Arrays.asList(roleIds));
        return new R(RetEnum.SERVICE_OK);
    }
}
