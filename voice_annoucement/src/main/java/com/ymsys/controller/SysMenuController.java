package com.ymsys.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.ymsys.entity.SysMenuEntity;
import com.ymsys.exception.RRException;
import com.ymsys.myenum.RetEnum;
import com.ymsys.service.SysMenuService;
import com.ymsys.utils.Constant;
import com.ymsys.utils.R;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 菜单管理
 *
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-13 16:25:20
 */
@RestController
public class SysMenuController extends AbstractController{
    @Autowired
    private SysMenuService sysMenuService;

    /**
     * 导航菜单
     */
    @GetMapping("/menu/nav")
    public R nav(){
        List<SysMenuEntity> menuList = sysMenuService.getUserMenuList(getUserId());
        return new R(RetEnum.SERVICE_OK,menuList);
    }

    /**
     * 所有菜单列表
     */
    @GetMapping("/menu/list")
    //@RequiresPermissions("sys:menu:list")
    public R list(){
        List<SysMenuEntity> menuList = sysMenuService.selectList(null);
        for(SysMenuEntity sysMenuEntity : menuList){
            SysMenuEntity parentMenuEntity = sysMenuService.selectById(sysMenuEntity.getParentId());
            if(parentMenuEntity != null){
                sysMenuEntity.setParentName(parentMenuEntity.getName());
            }
        }

        return new R(RetEnum.SERVICE_OK,menuList);
    }

    /**
     * 选择菜单(添加、修改菜单)
     */
    @GetMapping("/menu/select")
    //@RequiresPermissions("sys:menu:select")
    public R select(){
        //查询列表数据
        List<SysMenuEntity> menuList = sysMenuService.queryNotButtonList();

        //添加顶级菜单
        SysMenuEntity root = new SysMenuEntity();
        root.setMenuId(0L);
        root.setName("一级菜单");
        root.setParentId(-1L);
        root.setOpen(true);
        menuList.add(root);
        return new R(RetEnum.SERVICE_OK,menuList);
    }

    /**
     * 修改
     */
    @RequestMapping("/menu/update")
    //@RequiresPermissions("sys:menu:update")
    public R update(@RequestBody SysMenuEntity menu){
        //数据校验
        verifyForm(menu);
        sysMenuService.updateById(menu);

        return new R(RetEnum.SERVICE_OK);
    }
    /**
     * 菜单信息
     */
    @GetMapping("/menu/info/{menuId}")
    //@RequiresPermissions("sys:menu:info")
    public R info(@PathVariable("menuId") Long menuId){
        SysMenuEntity menu = sysMenuService.selectById(menuId);
        SysMenuEntity pMenu = sysMenuService.selectById(menu.getParentId());
        menu.setParentName(pMenu.getName());
        return new R(RetEnum.SERVICE_OK,menu);
    }

    /**
     * 保存
     */
    @RequestMapping("/menu/save")
    //@RequiresPermissions("sys:menu:save")
    public R save(@RequestBody SysMenuEntity menu){
        //数据校验
        verifyForm(menu);

        sysMenuService.insert(menu);

        return new R(RetEnum.SERVICE_OK);
    }

    /**
     * 删除
     */
    @RequestMapping("/mune/delete")
    //@RequiresPermissions("sys:menu:delete")
    public R delete(@RequestBody Map<String, Long> params){
        Long menuId = (Long)params.get("menuId");
        if(menuId <= 8){
            return new R(RetEnum.SERVICE_SYS_MUNE_CANNOT_DELETE);
        }

        //判断是否有子菜单或按钮
        List<SysMenuEntity> menuList = sysMenuService.queryListParentId(menuId);
        if(menuList.size() > 0){
            return new R(RetEnum.SERVICE_DELETE_SUB_MUNE_OR_BUTTON);
        }

        sysMenuService.delete(menuId);

        return new R(RetEnum.SERVICE_OK);
    }


    /**
     * 验证参数是否正确
     */
    private void verifyForm(SysMenuEntity menu){
        if(StringUtils.isBlank(menu.getName())){
            throw new RRException("菜单名称不能为空");
        }

        if(menu.getParentId() == null){
            throw new RRException("上级菜单不能为空");
        }

        //菜单
        if(menu.getType() == Constant.MenuType.MENU.getValue()){
            if(StringUtils.isBlank(menu.getUrl())){
                throw new RRException("菜单URL不能为空");
            }
        }

        //上级菜单类型
        int parentType = Constant.MenuType.CATALOG.getValue();
        if(menu.getParentId() != 0){
            SysMenuEntity parentMenu = sysMenuService.selectById(menu.getParentId());
            parentType = parentMenu.getType();
        }

        //目录、菜单
        if(menu.getType() == Constant.MenuType.CATALOG.getValue() ||
                menu.getType() == Constant.MenuType.MENU.getValue()){
            if(parentType != Constant.MenuType.CATALOG.getValue()){
                throw new RRException("上级菜单只能为目录类型");
            }
            return ;
        }

        //按钮
        if(menu.getType() == Constant.MenuType.BUTTON.getValue()){
            if(parentType != Constant.MenuType.MENU.getValue()){
                throw new RRException("上级菜单只能为菜单类型");
            }
            return ;
        }
    }
}
