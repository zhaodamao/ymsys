package com.ymsys.controller;

import com.ymsys.myenum.RetEnum;
import com.ymsys.service.*;
import com.ymsys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


/**
 * 角色
 *
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-13 16:25:20
 */
@RestController
public class SysFileController {
    @Autowired
    SysFileService sysFileService;
    /**
     * 删除角色
     */
    @PostMapping("/file/upload")
    public R fileUpload(@RequestParam("files") MultipartFile files[]){
        sysFileService.fileUpload(files);
        return new R(RetEnum.SERVICE_OK);
    }
}
