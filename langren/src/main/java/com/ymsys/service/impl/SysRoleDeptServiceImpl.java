package com.ymsys.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ymsys.dao.SysRoleDeptDao;
import com.ymsys.entity.SysRoleDeptEntity;
import com.ymsys.service.SysRoleDeptService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("sysRoleDeptService")
public class SysRoleDeptServiceImpl extends ServiceImpl<SysRoleDeptDao, SysRoleDeptEntity> implements SysRoleDeptService {
    @Override
    public List<Long> queryDeptIdList(Long[] roleIds) {
        return baseMapper.queryDeptIdList(roleIds);
    }

}
