package com.ymsys.service.impl;

import com.ymsys.dao.SysMenuDao;
import com.ymsys.entity.SysMenuEntity;
import com.ymsys.service.SysMenuService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;




@Service("sysMenuService")
public class SysMenuServiceImpl extends ServiceImpl<SysMenuDao, SysMenuEntity> implements SysMenuService {

    /*
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysMenuEntity> page = this.selectPage(
                new Query<SysMenuEntity>(params).getPage(),
                new EntityWrapper<SysMenuEntity>()
        );

        return new PageUtils(page);
    }
    */
}
