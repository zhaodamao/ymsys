package com.ymsys.service.impl;

import com.ymsys.dao.SysRoleMenuDao;
import com.ymsys.entity.SysRoleMenuEntity;
import com.ymsys.service.SysRoleMenuService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;



@Service("sysRoleMenuService")
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuDao, SysRoleMenuEntity> implements SysRoleMenuService {

    /*
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysRoleMenuEntity> page = this.selectPage(
                new Query<SysRoleMenuEntity>(params).getPage(),
                new EntityWrapper<SysRoleMenuEntity>()
        );

        return new PageUtils(page);
    }
    */

}
