package com.ymsys.service;


import com.baomidou.mybatisplus.service.IService;
import com.ymsys.entity.SysMenuEntity;


/**
 * 菜单管理
 *
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-13 16:25:20
 */
public interface SysMenuService extends IService<SysMenuEntity> {

    //PageUtils queryPage(Map<String, Object> params);
}

