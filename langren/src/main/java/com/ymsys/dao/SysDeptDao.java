package com.ymsys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ymsys.entity.SysDeptEntity;

import java.util.List;

/**
 * 部门管理
 * 
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-15 14:25:41
 */
public interface SysDeptDao extends BaseMapper<SysDeptEntity> {

    /**
     * 查询子部门ID列表
     * @param parentId  上级部门ID
     */
    List<Long> queryDetpIdList(Long parentId);
}
