package com.ymsys.controller.controller;

import java.util.Arrays;
import java.util.Map;

import com.ymsys.entity.SysMenuEntity;
import com.ymsys.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;




/**
 * 菜单管理
 *
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-13 16:25:20
 */
@RestController
@RequestMapping("/sysmenu")
public class SysMenuController {
    @Autowired
    private SysMenuService sysMenuService;

}
